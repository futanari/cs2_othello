#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {
private:
    Side side;
    Board board;
    static int weights[8][8]; 
    
public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    int heuristic(int x, int y);
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
