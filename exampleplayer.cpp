#include "exampleplayer.h"

int ExamplePlayer::weights[8][8] = {
         { 50, 0,  1,  1,  1,  1, 0, 50},
         { 0,  1,  1,  1,  1,  1,  1, 0},
         {  1,  1,  1,  1,  1,  1,  1,  1},
         {  1,  1,  1,  1,  1,  1,  1,  1},
         {  1,  1,  1,  1,  1,  1,  1,  1},
         {  1,  1,  1,  1,  1,  1,  1,  1},
         { 0,  1,  1,  1,  1,  1,  1, 0},
         { 50, 0,  1,  1,  1,  1, 0, 50}
    };

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side Sside) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    board=Board();
    side=Sside; 
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    Side other = (side == BLACK) ? WHITE : BLACK;
    
    if(opponentsMove!=NULL) {
        board.doMove(opponentsMove,other);
    }

    if(!board.hasMoves(side)){return NULL;}
    
    int currx=0; int curry=0;
    int currscore=-100000;
    Move *testMove;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            testMove = new Move(i,j);
            if(board.checkMove(testMove, side))
            {
                int H=this->heuristic(i,j);
                if(H>currscore)
                {
                    currx=i; curry=j; currscore=H;
                }
            }
            delete testMove;
        }
    }

    testMove = new Move(currx, curry);
    board.doMove(testMove, side);
    return testMove;
    
}

int ExamplePlayer::heuristic(int x, int y)
{
    Side other = (side == BLACK) ? WHITE : BLACK;
    Board* boardc= board.copy();
    Move *testMove = new Move(x,y);
    boardc->doMove(testMove, side);
    
    int score = weights[x][y] * ((boardc->count(side))-(boardc->count(other)));
    
    delete boardc;
    delete testMove;

    return score;
}



































